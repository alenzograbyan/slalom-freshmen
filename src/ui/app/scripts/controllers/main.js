'use strict';

/**
 * @ngdoc function
 * @name hackalongApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the hackalongApp
 */
angular.module('hackalongApp')
  .controller('MainCtrl', function ($scope, $http, $window, jwtHelper) {

    /* jshint validthis: true */
    $scope.sessionId = null;
    $scope.sessionCreateDate = null;

    $scope.team = [
      {name: 'Alen'},
      {name: 'David'},
      {name: 'Randy'},
      {name: 'Tyler'}
    ];

    //TODO: ng-repeat the technologies in the html
    $scope.technologies = [];

    $scope.callAPI = function() {
      $http
        .get('https://slalomfreshmen.azurewebsites.net/api/usersession')
        //.success(function (data, status, headers, config) {
        .success(function (data) {
          $window.sessionStorage.token = data.token;
          console.log('token is ');
          console.log(data.token);

          var tokenPayload = jwtHelper.decodeToken(data.token);
          console.log('payload is ');
          console.log(tokenPayload);
          $scope.sessionId = tokenPayload.SlalomFreshmentUserId;
          $scope.sessionCreateDate = tokenPayload.formattedExpiration;
        })
        //.error(function (data, status, headers, config) {
        .error(function () {
          // Erase the token if the user fails to log in
          delete $window.sessionStorage.token;

          // Handle login errors here
        });
    };
  });
