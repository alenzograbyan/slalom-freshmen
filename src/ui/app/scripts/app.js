'use strict';

/**
 * @ngdoc overview
 * @name hackalongApp
 * @description
 * # hackalongApp
 *
 * Main module of the application.
 */
angular
  .module('hackalongApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angular-jwt'
  ])
  .config(function ($routeProvider, $httpProvider) {
    $httpProvider.interceptors.push('tokenInterceptor');

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
